package com.bus.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bus.model.Booking;
@Repository
public interface BusBookingRepo extends JpaRepository<Booking, Integer> {
	
	List<Booking> findByJourneyDate(Date date);

}
