package com.bus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bus.dto.BusDto;
import com.bus.dto.CreateBusDto;
import com.bus.dto.ResponseMessageDto;
import com.bus.model.Bus;
import com.bus.service.BusService;
import com.bus.service.UserService;

@RestController
public class BusController {

	@Autowired
	BusService busService;
	@Autowired
	UserService userService;
	

	

	@PostMapping(value="/bus")
	public ResponseEntity<ResponseMessageDto> createBus(@RequestBody CreateBusDto createBusDto, @RequestParam("email") String email) {
		ResponseMessageDto dto = new ResponseMessageDto();
		
		if(userService.finduserByEmail(email)) {
		 busService.createBus(createBusDto);
		
		dto.setMessage("created sucessfully");
		return  new ResponseEntity<>(dto,HttpStatus.OK);
	}
		else {
			dto.setMessage("no access for you");
			return  new ResponseEntity<>(dto,HttpStatus.BAD_REQUEST);
		
		}
		}
	
	@PutMapping(value="/bus")
	public ResponseEntity<ResponseMessageDto> updateBus(@RequestBody CreateBusDto createBusDto, @RequestParam("email") String email) {
		ResponseMessageDto dto = new ResponseMessageDto();
		if(userService.finduserByEmail(email)) {
		 busService.createBus(createBusDto);
		
		dto.setMessage("Updated sucessfully");
		return  new ResponseEntity<>(dto,HttpStatus.OK);
	}
		else {
			dto.setMessage("no access for you");
			return  new ResponseEntity<>(dto,HttpStatus.BAD_REQUEST);
		
		}
	
	}
	
	@DeleteMapping(value="/bus/{id}")
	public ResponseEntity<ResponseMessageDto> deleteBus(@PathVariable int id, @RequestParam("email") String email) {
		ResponseMessageDto dto = new ResponseMessageDto();
		if(userService.finduserByEmail(email)) {
		 busService.deleteBus(id);
		
		dto.setMessage("bus with"+id+"deleted sucessfully");
		return  new ResponseEntity<>(dto,HttpStatus.OK);
	}
		else {
			dto.setMessage("no access for you");
			return  new ResponseEntity<>(dto,HttpStatus.BAD_REQUEST);
		
		}
	}
		
	
	@GetMapping(value="/buses")
	public ResponseEntity<Object>  getBus(@RequestParam("email") String email) {
		ResponseMessageDto dto = new ResponseMessageDto();
		if(userService.finduserByEmail(email)) {
		List<Bus> buses = busService.getBus();
		
		
		return  new ResponseEntity<>(buses,HttpStatus.OK);
	}
		else {
			dto.setMessage("no access for you");
			return  new ResponseEntity<>(dto,HttpStatus.BAD_REQUEST);
		
		}
	}
	

	@PostMapping(value="/buses")
	public ResponseEntity<Object> searchbus(@RequestBody BusDto busDto,@RequestParam("email") String email) {
		ResponseMessageDto dto = new ResponseMessageDto();
		if(userService.finduserByEmail(email)) {
		List<Bus> buses = busService.searchbus(busDto);
		
		
		return  new ResponseEntity<>(buses,HttpStatus.OK);
	}
		else {
			dto.setMessage("no access for you");
			return  new ResponseEntity<>(dto,HttpStatus.BAD_REQUEST);
		
		}
	}
}