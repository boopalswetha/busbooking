package com.bus.service;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bus.dto.BookingDto;
import com.bus.exception.DataNotFound;
import com.bus.exception.SeatsNotAvilableException;
import com.bus.exception.TransactionFailedException;
import com.bus.exception.UserNotfoundException;
import com.bus.model.Booking;
import com.bus.model.Bus;
import com.bus.model.User;
import com.bus.repository.BusBookingRepo;
import com.bus.repository.BusRepo;
import com.bus.repository.UserRepo;

@Service
public class BusBookingServiceImpl implements BusBookingService {

	@Autowired
	private BusBookingRepo busBookingRepo;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private BusRepo busRepo;
	@Autowired
	private UserRepo userRepo;

	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Override
	public Booking makeBooking(BookingDto bookingDto) {
		Booking booking = new Booking();
		BeanUtils.copyProperties(bookingDto, booking);

		User user = userRepo.findByEmail(bookingDto.getEmailId());
		if (user != null) {

			Bus bus = busRepo.findById(bookingDto.getBusId()).orElseThrow(() -> new DataNotFound());

			if (bus.getNoOfSeatsAvailable() > bookingDto.getNoOfSeatsBooked()) {

				bus.setNoOfSeatsAvailable(bus.getNoOfSeatsAvailable() - bookingDto.getNoOfSeatsBooked());
				busRepo.save(bus);
				double fare = bus.getFare() * bookingDto.getNoOfSeatsBooked();

				booking.setAmount(fare);
				ResponseEntity<String> resposne = this.transction(bookingDto, fare);
				if (resposne.getStatusCode() == HttpStatus.OK) {
					booking.setUser(user);
					booking.setBus(bus);
					booking.setJourneyDate(bus.getJourneyDate());

					return busBookingRepo.save(booking);
				} else {
					throw new TransactionFailedException();
				}
			} else {
				throw new SeatsNotAvilableException();
			}
		} else {
			throw new UserNotfoundException("requested user is not there");
		}

	}

	public ResponseEntity<String> transction(BookingDto bookingDto, double sum) {
		String uri = "http://localhost:8081/transaction";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("accountNumber", bookingDto.getAccountNumber());
		request.put("amount", sum);
		request.put("banificiaryAccountNo", 6666);

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		return response;
	}

	@Override
	public List<Booking> getBooking(Date date) {

		return busBookingRepo.findByJourneyDate(date);
	}

}
