package com.bus.service;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bus.dto.BusDto;
import com.bus.dto.CreateBusDto;
import com.bus.exception.BusNotFoundException;
import com.bus.model.Bus;
import com.bus.repository.BusRepo;


@Service
public class BusServiceImpl implements BusService {

	@Autowired
	BusRepo busRepository;

	@Override
	public Bus createBus(CreateBusDto createBusDto) {
		Bus bus=new Bus();
		BeanUtils.copyProperties(createBusDto, bus);
		
		return busRepository.save(bus);
	}

	@Override
	public List<Bus> searchbus(BusDto busDto) throws BusNotFoundException {
		
		
		
		List<Bus> buses = busRepository.findBusBySourceAndDestinationAndJourneyDate(busDto.getSource(),busDto.getDestination(),busDto.getJourneyDate());
		if(buses.isEmpty()) {
			throw new BusNotFoundException("buses are not available");
		}
		return buses;
	}



	@Override
	public int deleteBus(int id) {
		
		
		 try {
			busRepository.deleteById(id);
		} catch (Exception e) {
			throw new BusNotFoundException("the requested bus is not there");
		}
		 return id;
	}

	@Override
	public List<Bus> getBus() {
		List<Bus> buses =busRepository.findAll();
		
		if(buses.isEmpty()) {
			throw new BusNotFoundException("buses are not available");
		}
		
		return buses;
	}

}
