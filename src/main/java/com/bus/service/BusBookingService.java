package com.bus.service;

import java.util.Date;
import java.util.List;

import com.bus.dto.BookingDto;
import com.bus.model.Booking;

public interface BusBookingService {

	Booking makeBooking(BookingDto bookingDto);

	List<Booking> getBooking(Date date);

}
