package com.bus.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bus.dto.UserLoginDto;
import com.bus.exception.UserNotfoundException;
import com.bus.model.User;


import com.bus.repository.UserRepo;


@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserRepo loginRepository;

	@Override
	public User login(UserLoginDto userLoginDto) throws UserNotfoundException   {
	


		User login = loginRepository.findByEmailAndPassword(userLoginDto.getEmail(), userLoginDto.getPassword());
		if (login==null) {
			throw new UserNotfoundException("User doesnot exists");
		}
		
		
		return login;
		
	}
}
