package com.bus.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bus.dto.ResponseMessageDto;
import com.bus.dto.UserLoginDto;
import com.bus.exception.UserNotfoundException;
import com.bus.model.User;
import com.bus.model.UserLogin;
import com.bus.service.LoginService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;
	@Mock
	LoginService loginService;

	@Test
	public void testFindByUserNameAndPasswordForPositive() throws UserNotfoundException {
		User userLogin = new User();
		UserLoginDto userDto = new UserLoginDto();
		Mockito.when(loginService.login(userDto)).thenReturn(userLogin);
		ResponseEntity<ResponseMessageDto> user1 = loginController.login(userDto);
		Assert.assertNotNull(user1);
		Assert.assertEquals(HttpStatus.OK, user1.getStatusCode());

	}

	@Test
	public void testFindByCustomerIdAndPasswordForNegative() throws UserNotfoundException {
		User userLogin = new User();
		UserLoginDto userDto = new UserLoginDto();
		Mockito.when(loginService.login(userDto)).thenReturn(userLogin);
		ResponseEntity<ResponseMessageDto> user1 = loginController.login(userDto);
		Assert.assertNotNull(user1);
		Assert.assertEquals(HttpStatus.OK, user1.getStatusCode());

	}
}
